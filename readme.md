# My Tool for the Automation of Creating Facial Recognition Datasets

- [My Tool for the Automation of Creating Facial Recognition Datasets](#my-tool-for-the-automation-of-creating-facial-recognition-datasets)
  - [About](#about)
    - [Trained Model](#trained-model)
    - [Example](#example)
  - [Tool Output](#tool-output)
  - [User Guide](#user-guide)
    - [Commandline Arguments](#commandline-arguments)
    - [Using the User Interface](#using-the-user-interface)
      - [GUI Buttons & Fields](#gui-buttons--fields)
    - [Example](#example-1)
  - [Appendix](#appendix)
    - [A1. Using a custom trained model](#a1-using-a-custom-trained-model)
    - [A2. Intersection Over Union (IoU)](#a2-intersection-over-union-iou)
    - [A3. Names file](#a3-names-file)
  - [Future Improvement Ideas](#future-improvement-ideas)

## About

This repository hosts a small project that branched from some other projects that I've been working on -- [PyTorch FasterRCNN trained on Wider](https://gitlab.com/dibz15/pytorch-fasterrcnn-trained-on-wider) and [PyTorch Facial Recognition Model](https://gitlab.com/dibz15/pytorch-facial-recognition-model).

Basically, it's a simple PyQt GUI interface that helps to automate the creation of custom facial-recognition datasets. It seems that it could potentially be extended to other types of classification datasets, but my intention was for use with faces. 

### Trained Model

With this project, you're free to fork and modify it to work with any object detection model, but it is currently written to work with the output of a FasterRCNN network. A pre-trained model (trained on the WIDER faces dataset) is provided in this repository under `trained_model.tar.gz`. The unzipped model is about 320MB.

Easy-to-run example:

    python runExtractorGUI.py  --cuda --model_file combined_state_final.pth --in_type v --input_file ./sample_media/hobbits.mp4 --names_file ./sample_media/names.txt

Note that the default output path is at `./dataset` so that is where it will produce files.

### Example

Given an image or video input, it is easy to 'extract' and classify faces (or other detected objects) using this tool. 

So, say I'd like to train the dataset to recognize a familiar face (highlighted to show the extracted region of each frame):

<!-- ![Obi-wan annotated](./sample_media/obi-wan_annotated.gif) -->
<img src="./sample_media/obi-wan_annotated.gif" style="width: 100%; max-width: 512px"/>

Then I can run the GUI with the following commandline:

    python runExtractorGUI.py  --cuda --model_file combined_state_final.pth --in_type v --input_file ./sample_media/obi-wan.mp4 --names_file ./sample_media/names.txt

And then after a few steps (instructions are at [Using the User Interface](#using-the-user-interface)) we have a folder output like the following:

<!-- ![Obi-wan output](./sample_media/obi-wan_output.gif) -->
<img src="./sample_media/obi-wan_output.gif" style="width: 100%; max-width: 1080px"/>

-----

## Tool Output

The output directory structure of this tool is intented to be compatible with the PyTorch ImageFolder or DatasetFolder classes to make it easy to use the output for image classification training and inference. 

As given in the PyTorch [source for folder.py](https://github.com/pytorch/vision/blob/main/torchvision/datasets/folder.py), the output sub-structure looks something like this:

    root/dog/xxx.png
    root/dog/xxy.png
    root/dog/[...]/xxz.png

    root/cat/123.png
    root/cat/nsdf3.png
    root/cat/[...]/asd932_.png

In the context of this tool, the default output 'root' is one of three folders under `./dataset` (`train`, `val`, or `test`) but the location can be easily set using the `--output_root` argument (this is explained in more detail below). Under the root, a folder for `test`, `train`, and `val` are all produced. Under each of these subfolders, the structure given above for `ImageFolder` is followed, where each class gets a folder with images.

Then, in your classification script, you would have something like this:

    train_dataset = torchvision.datasets.ImageFolder(root='./dataset/train')
    val_dataset = trochvision.datasets.ImageFolder(root='./dataset/val')

-----

## User Guide

Using the tool is pretty simple, and it lacks any very advanced features. However, so that it may be useful to others I am providing a simple user guide here so that someone else may benefit from using this tool.

### Commandline Arguments

    usage: runExtractorGUI.py [-h] [--cuda] --model_file MODEL_FILE
                [--score_threshold SCORE_THRESHOLD]
                [--iou_threshold IOU_THRESHOLD]
                [--model_input_size MODEL_INPUT_SIZE] --input_file
                INPUT_FILE [--in_type IN_TYPE]
                [--output_root OUTPUT_ROOT]
                [--names_file NAMES_FILE]

Above is the output of `python runExtractorGUI.py -h`, but I want to give a more careful breakdown of the commandline arguments here.

Required:
- `--model_file`: Path to a trained PyTorch FasterRCNN object detection model's weights (usually a .pth file?). I provide such a model for facial detection in `trained_model.tar.gz`. If you wish to use your own, read about the requirements in the [Appendix A1](#a1-using-a-custom-trained-model).
- `--input_file`: Path to the input media. This can be an image (`.jpeg`, `.png`, etc.) or a video file (`.mp4` or other h.264 codec media formats). Note that for video you need to also set `--in_type v`.

Optional:
- `--names_file`: Default -- `./names.txt`. File of list of names/object nouns. See [Appendix A3](#a3-names-file).  
- `--output_root`: Default -- `./dataset`. See [Tool Output](#tool-output) for more info.
- `--cuda`: Flag to tell the script to use CUDA, if possible.
- `--score_threshold`: Default -- 0.5. Threshold for the confidence of face detection predictions. Default 0.5 means a box will be produced only if the model is 50% confident or greater.
- `--iou_threshold`: Default -- 0.4. Intersection over union score threshold. 0.5 is pretty good, higher makes it harder to match but means the model is more accurate. See [Appendix A2](#a2-intersection-over-union-iou).
- `--model_input_size`: Default -- 512. The max dimension (width or height) size that will be run through the detection model. If the input media is larger than this, it will be scaled down. Decreasing this value will improve runtime performance, but will likely decrease detection accuracy. 

### Using the User Interface


<!-- ![Using the interface example](./sample_media/user_interface_example.gif) -->
<img src="./sample_media/user_interface_example.gif" style="width: 100%; max-width: 1080px"/>

#### GUI Buttons & Fields

- `Select Name` Field: This is a dropdown where you choose the 'name' (class, object noun, etc.) of the currently focused found object. This list is populated from the provided `names.txt` file or can be added manually using the `Add Name` field.
- `Add Name` Field: Text for a new name can be entered here, and then is added to the `Select Name` drop-down when you hit `Add`.
- `Type` Field: This drop-down allows you to choose where this example should be placed: `train`, `val`, or `test`. This determines the sub-folder for the example and can be used to build separate training and testing datasets.
- `Update Scale` Button: This is used to scale up/down the extracted image examples before they're exported, using the field to the left of this button. By default (scale is 1.0) they are clipped from the downscaled image that is supplied to the model. By specifying scale != 1.0, then these will be clipped from the original image (may be larger than what is fed into the model).
- `Save` Button: Saves the displayed clipped example into the displayed path.
- `Next` Button: Sets the focused clip onto the next face in the image, or goes to the next frame of the video
- `Save and Next` Button: Saves the displayed image, and goes to the next face/frame.
- `Extract All` Button: Uses the current settings and automatically goes through every frame/face and saves them for the whole video. Not recommended for multi-class examples.

### Example

<!-- ![Using the interface example 2](./sample_media/user_interface_example2.gif) -->
<img src="./sample_media/user_interface_example2.gif" style="width: 100%; max-width: 1080px"/>


Steps to reproduce the above example:

1. `python runExtractorGUI.py  --cuda --model_file combined_state_final.pth --in_type v --input_file ./sample_media/hobbits.mp4  --names_file ./sample_media/names.txt`
2. The first image is Frodo, so under `Select Name` we select `Frodo`.
3. Set the type (`train`, `val`, `test`).
4. Change the scale to 1.3 for higher resolution output.
5. Click `Save and Next`. Repeat steps 2-5 for each face of each frame. 
6. If you don't wish to save an image, clicking `Next` will proceed without saving. 
7. It can be good to have a negative matching example, so `unknown` can be used for unnamed faces.

-----

## Appendix


### A1. Using a custom trained model

Since this tool could be useful for training classifiers for types of objects other than faces, it makes sense to provide a provision for trying out other trained object detection models.

There are two options for loading and running a custom model. Either it can be loaded in as an entire model from disk (all model information stored on disk), or it can be loaded from the model state-dict.

If loading a full model, then the script just expects the *.pth file to hold a `dict` object with a single key: `'full_model'` that has the entire model binary.

If running from the model state-dict (this may be smaller on disk) then the *.pth file must have a `dict` object with two keys: `'model_state'` and `'backbone_state'`. 

You can read more about how I produced this model [here](https://gitlab.com/dibz15/pytorch-fasterrcnn-trained-on-wider).

These are the state-dicts of the model and its backbone (FasterRCNN in PyTorch requires a backbone specified, in my case it is a MobileNetV2). So if you wish to load a model via its weights, then it must have the same structure.

### A2. Intersection Over Union (IoU)

Intersection over Union bounding box comparison considers a bounding box overlapping another bounding box if their 'IoU' value is greather than some threshold. For IoU threshold 0.4 this means that if the predicted box is overlapping the ground truth by at least 40% then it is considered a true positive. If a box overlaps less than 40% it is a false positive, and if there is a ground truth box without an overlapping predicted box then it is a false negative. Everything else would be a true negative.

Read more [here](https://towardsdatascience.com/intersection-over-union-iou-calculation-for-evaluating-an-image-segmentation-model-8b22e2e84686).

The threshold for this IoU calculation is set with the commandline argument `--iou_threshold`. See [Commandline Arguments](#commandline-arguments). 
A useful comparison measurement for object detection models is mAP (mean average precision) of a model. Often mAP is charted vs. IoU.

Read more about mAP [here](https://towardsdatascience.com/implementation-of-mean-average-precision-map-with-non-maximum-suppression-f9311eb92522).

See my own model's performance curve at [PyTorch FasterRCNN trained on Wider](https://gitlab.com/dibz15/pytorch-fasterrcnn-trained-on-wider).

### A3. Names file

The names file (default `names.txt`) stores class/object names as a simple newline-separated list. 

Here's the contents of the `names.txt` that I've used in my examples, you can find it in `./sample_media/names.txt`:

    conan
    gandalf
    obi-wan
    merry
    pippin
    frodo

  These names will show up in the name combo box for selection. 

## Future Improvement Ideas

This GUI is helpful but very basic, so there are definitely some improvements that could be developed. I'm not sure I'll have time for it, but here are some ideas for others in the future:

- Add file selection browser rather than file command line argument.
- Add ability to open a folder of images/media, so it will continue from one media file to the next.
- Names added via `Add Name` could be appended to the provided `names.txt`.