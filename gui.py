# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'guiPlSBXT.ui'
##
## Created by: Qt User Interface Compiler version 5.14.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PyQt5.QtCore import (QCoreApplication, QDate, QDateTime, QMetaObject,
    QObject, QPoint, QRect, QSize, QTime, QUrl, Qt)
from PyQt5.QtWidgets import *


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(558, 373)
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.lineEdit = QLineEdit(self.centralwidget)
        self.lineEdit.setObjectName(u"lineEdit")
        self.lineEdit.setGeometry(QRect(10, 30, 81, 24))
        self.faceLabel = QLabel(self.centralwidget)
        self.faceLabel.setObjectName(u"faceLabel")
        self.faceLabel.setGeometry(QRect(340, 0, 211, 251))
        self.comboBox = QComboBox(self.centralwidget)
        self.comboBox.setObjectName(u"comboBox")
        self.comboBox.setGeometry(QRect(10, 80, 171, 31))
        self.label = QLabel(self.centralwidget)
        self.label.setObjectName(u"label")
        self.label.setGeometry(QRect(10, 10, 81, 16))
        self.addNameBtn = QPushButton(self.centralwidget)
        self.addNameBtn.setObjectName(u"addNameBtn")
        self.addNameBtn.setGeometry(QRect(100, 30, 80, 23))
        self.label_2 = QLabel(self.centralwidget)
        self.label_2.setObjectName(u"label_2")
        self.label_2.setGeometry(QRect(10, 60, 71, 16))
        self.submitBtn = QPushButton(self.centralwidget)
        self.submitBtn.setObjectName(u"submitBtn")
        self.submitBtn.setGeometry(QRect(10, 250, 81, 31))
        self.pathLabel = QLabel(self.centralwidget)
        self.pathLabel.setObjectName(u"pathLabel")
        self.pathLabel.setGeometry(QRect(10, 230, 171, 16))
        self.nextFaceBtn = QPushButton(self.centralwidget)
        self.nextFaceBtn.setObjectName(u"nextFaceBtn")
        self.nextFaceBtn.setGeometry(QRect(100, 250, 81, 31))
        self.typeCombo = QComboBox(self.centralwidget)
        self.typeCombo.setObjectName(u"typeCombo")
        self.typeCombo.setGeometry(QRect(50, 130, 131, 31))
        self.label_3 = QLabel(self.centralwidget)
        self.label_3.setObjectName(u"label_3")
        self.label_3.setGeometry(QRect(10, 140, 31, 16))
        self.extractAllBtn = QPushButton(self.centralwidget)
        self.extractAllBtn.setObjectName(u"extractAllBtn")
        self.extractAllBtn.setGeometry(QRect(380, 290, 131, 31))
        self.extractAllBtn.setAutoFillBackground(False)
        self.scaleLineEdit = QLineEdit(self.centralwidget)
        self.scaleLineEdit.setObjectName(u"scaleLineEdit")
        self.scaleLineEdit.setGeometry(QRect(10, 190, 41, 24))
        self.scaleButton = QPushButton(self.centralwidget)
        self.scaleButton.setObjectName(u"scaleButton")
        self.scaleButton.setGeometry(QRect(60, 190, 121, 31))
        self.labelOutSize = QLabel(self.centralwidget)
        self.labelOutSize.setObjectName(u"labelOutSize")
        self.labelOutSize.setGeometry(QRect(420, 250, 111, 21))
        self.label_5 = QLabel(self.centralwidget)
        self.label_5.setObjectName(u"label_5")
        self.label_5.setGeometry(QRect(360, 250, 49, 16))
        self.submitBtnNext = QPushButton(self.centralwidget)
        self.submitBtnNext.setObjectName(u"submitBtnNext")
        self.submitBtnNext.setGeometry(QRect(250, 290, 121, 31))
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QMenuBar(MainWindow)
        self.menubar.setObjectName(u"menubar")
        self.menubar.setGeometry(QRect(0, 0, 558, 21))
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QStatusBar(MainWindow)
        self.statusbar.setObjectName(u"statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)

        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"MainWindow", None))
        self.faceLabel.setText(QCoreApplication.translate("MainWindow", u"TextLabel", None))
        self.label.setText(QCoreApplication.translate("MainWindow", u"Add Name", None))
        self.addNameBtn.setText(QCoreApplication.translate("MainWindow", u"Add", None))
        self.label_2.setText(QCoreApplication.translate("MainWindow", u"Select Name", None))
        self.submitBtn.setText(QCoreApplication.translate("MainWindow", u"Save", None))
        self.pathLabel.setText(QCoreApplication.translate("MainWindow", u"TextLabel", None))
        self.nextFaceBtn.setText(QCoreApplication.translate("MainWindow", u"Next", None))
        self.label_3.setText(QCoreApplication.translate("MainWindow", u"Type", None))
        self.extractAllBtn.setText(QCoreApplication.translate("MainWindow", u"Extract All", None))
        self.scaleButton.setText(QCoreApplication.translate("MainWindow", u"Update Scale", None))
        self.labelOutSize.setText(QCoreApplication.translate("MainWindow", u"TextLabel", None))
        self.label_5.setText(QCoreApplication.translate("MainWindow", u"Size:", None))
        self.submitBtnNext.setText(QCoreApplication.translate("MainWindow", u"Save and Next", None))
    # retranslateUi

