from ctypes import resize
from PyQt5 import QtWidgets, QtGui, QtCore
from PyQt5.QtGui import QImage, QPixmap
from gui import Ui_MainWindow

import sys
import os
import random

import cv2

from utils import *
import numpy as np

def convertImgToQt(rgbImage):
    # https://stackoverflow.com/a/55468544/6622587
    rgbImage = cv2.cvtColor(rgbImage, cv2.COLOR_BGR2RGB)
    h, w, ch = rgbImage.shape
    bytesPerLine = ch * w
    convertToQtFormat = QImage(rgbImage.data.tobytes(), w, h, bytesPerLine, QImage.Format_RGB888)

    return convertToQtFormat.scaled(200, 200, QtCore.Qt.KeepAspectRatio)

def extractFaceImages(frame, boxes, scale=1.0):
    extractedImages = []
    for i in range(len(boxes)):
        y1, y2 = int(round(boxes[i][1] * scale)), int(round(boxes[i][3] * scale))
        x1, x2 = int(round(boxes[i][0] * scale)), int(round(boxes[i][2] * scale))
        extractedImg = frame[y1:y2, x1:x2]
        # cv2.imshow('Face{}'.format(i), extractedImg)
        extractedImages.append(extractedImg)

    return extractedImages

class MyGUI(QtWidgets.QMainWindow):
    def __init__(self, dataroot, model, media, 
                    names_list=[], in_type='img', device='cpu', 
                    score_threshold=0.5, iou_threshold=0.2, model_input_size=512):

        super(MyGUI, self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        
        self.dataroot = dataroot
        self.model = model
        self.media = media
        self.in_type = in_type
        self.device = device

        self.score_threshold = score_threshold
        self.iou_threshold = iou_threshold

        self.model_input_size = model_input_size

        self.outputScale = 1.0

        self.ui.addNameBtn.clicked.connect(self.addName)
        self.ui.submitBtn.clicked.connect(self.submitName)
        self.ui.nextFaceBtn.clicked.connect(self.nextFace)
        self.ui.submitBtnNext.clicked.connect(self.submitAndNext)
        self.ui.comboBox.currentIndexChanged.connect(self.selectionChanged)
        self.ui.typeCombo.currentIndexChanged.connect(self.selectionChanged)
        self.ui.extractAllBtn.clicked.connect(self.extractAll)
        self.ui.scaleLineEdit.setText("1.0")
        self.ui.scaleButton.clicked.connect(self.updateScale)
        self.setFaceSizeLabel(0, 0)

        builtin_names = [
            "unknown"
        ]

        self.addNames(builtin_names)
        self.addNames(names_list)

        types = [
            "train",
            "val",
            "test"
        ]

        for t in types:
            self.ui.typeCombo.addItem(t)
        self.ui.typeCombo.setCurrentIndex(0)

        self.init()

        self.faceIterator = iter(self.faces)
        self.currentFace = None
        self.nextFace()

    def init(self):
        if self.in_type == 'img':
            image = self.media
            (ih, iw) = image.shape[:2]
            proportion = self.model_input_size / max(ih, iw)
            self.ow, self.oh =  int(iw * proportion), int(ih * proportion)

            self.currentImage = image
            self.processFrame(image)

        elif self.in_type == 'v':
            cap = self.media
            iw = int(cap.get(3))
            ih = int(cap.get(4))
            proportion = self.model_input_size / max(ih, iw) 
            self.ow, self.oh =  int(iw * proportion), int(ih * proportion)

            if cap.isOpened():
                ret, frame = cap.read()
                if ret:
                    self.currentImage = frame
                    self.processFrame(frame)
            else:
                print('Error opening video capture.')
                exit(1)

    def processFrame(self, frame):
        resizedImage = cv2.resize(frame, (self.ow, self.oh))

        modelInput = [getTestTransforms()(resizedImage).to(self.device)]
        pred = self.model(modelInput)
        sortedDict = sortPredictionsByClass(pred, 1)

        boxes = np.ndarray((0, 6))
        for k in sortedDict.keys():
            boxes = np.row_stack([boxes, nonMaximumSuppression_fast(sortedDict[k], confThresh=self.score_threshold, iouThresh=self.iou_threshold)])

        if len(boxes) == 0:
            self.faces = []
            return

        if self.outputScale != 1.0:
            outputImg = cv2.resize(frame, (int(round(self.ow * self.outputScale)), int(round(self.oh * self.outputScale))))
            print(outputImg.shape)
        else:
            outputImg = resizedImage

        extractedFaces = extractFaceImages(outputImg.copy(), boxes, self.outputScale)
        self.faces = extractedFaces

        cvPutBoxes(resizedImage, boxes)
        cv2.imshow('Frame', resizedImage)

    def getNextFrame(self):
        if self.media.isOpened():
            ret, frame = self.media.read()
            if ret:
                return frame
        
        return None

    def addName(self):
        name = self.ui.lineEdit.text().strip()
        if len(name) == 0:
            return
        self.ui.comboBox.addItem(name.lower())
        self.ui.lineEdit.clear()
    
    def addNames(self, names):
        for name in names:
            self.ui.comboBox.addItem(name.lower())

    def setFaceSizeLabel(self, w, h):
        self.ui.labelOutSize.setText(f"W:{w}, H:{h}")

    def nextFace(self):
        try:
            face = next(self.faceIterator)
            self.currentFace = face
            self.setImage(convertImgToQt(face))
            self.setFaceSizeLabel(self.currentFace.shape[0], self.currentFace.shape[1])
        except StopIteration:
            if self.in_type == 'img':
                self.ui.faceLabel.setPixmap(QPixmap())
                self.currentFace = None
                self.setFaceSizeLabel(0, 0)
            else:
                newFrame = self.getNextFrame()
                if newFrame is not None:
                    self.currentImage = newFrame
                    self.processFrame(newFrame)
                    self.faceIterator = iter(self.faces)
                    self.nextFace()
                else:
                    self.ui.faceLabel.setPixmap(QPixmap())
                    self.currentFace = None
                    self.setFaceSizeLabel(0, 0)

    def extractAll(self):
        while self.currentFace is not None:
            self.submitName()
            self.nextFace()
        
        print('Done grabbing all faces.')

    def submitName(self):
        os.makedirs(self.currentPath, exist_ok=True)
        if self.currentFace is not None:
            fullPath = os.path.join(
                        self.currentPath,
                        '{}_{}.jpg'.format(getTimestamp(), random.randint(1, 5000)))
            cv2.imwrite(fullPath, self.currentFace)
            print(f'Saved image to {fullPath}.')

    def submitAndNext(self):
        self.submitName()
        self.nextFace()

    def selectionChanged(self):
        self.currentPath = os.path.join(self.dataroot, 
                                        self.ui.typeCombo.currentText(),
                                        self.ui.comboBox.currentText())
        self.ui.pathLabel.setText(self.currentPath)

    def setImage(self, qtImage):
        self.ui.faceLabel.setPixmap(QPixmap.fromImage(qtImage))

    def updateScale(self):

        try:
            self.outputScale = float(self.ui.scaleLineEdit.text().strip())
            self.processFrame(self.currentImage)
            self.currentFace = None
            self.faceIterator = iter(self.faces)
            self.nextFace()
        except ValueError:
            self.ui.scaleLineEdit.setText(str(self.outputScale))
            print('Invalid scale float.')

if __name__ == "__main__":
    app = QtWidgets.QApplication([])
    application = MyGUI('dataset')

    application.show()

    sys.exit(app.exec())