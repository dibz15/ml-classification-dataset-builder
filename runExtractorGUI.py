import torch

from PIL import Image
import sys

from utils import *
import cv2

import argparse
from PyQt5 import QtWidgets
from extractor_gui import MyGUI

parser = argparse.ArgumentParser(description='FasterRCNN with MobileNetV2 on WIDER Face Dataset.')

#PyTorch Arguments
parser.add_argument('--cuda', action='store_true', help='use cuda?')
parser.add_argument('--model_file', type=str, required=True, help="pre-trained model file")
parser.add_argument('--score_threshold', type=float, default=0.5, help="Threshold for box scoring.")
parser.add_argument('--iou_threshold', type=float, default=0.4, help="Threshold for box scoring.")

#OpenCV arguments
parser.add_argument('--model_input_size', type=float, default=512, help="Amount by which to downscale dataset images.")
parser.add_argument('--input_file', type=str, required=True, help="image to run model on")
parser.add_argument('--in_type', type=str, default='img', help='Input type. \'v\' for video, \'img\' for image')

#Dataset arguments
parser.add_argument('--output_root', type=str, default='dataset', help='Root folder for dataset to be output')
parser.add_argument('--names_file', type=str, default='names.txt', help='File for names to add to the names list')

#Parse Arguments
opt = parser.parse_args()

with open(opt.names_file) as f:
    namesList = [line.rstrip() for line in f.readlines()]

# output_path = os.path.join(opt.output_root, opt.output_type, opt.cls)

#Load and prepare model ====================
device = torch.device('cuda:0') if opt.cuda and torch.cuda.is_available() else torch.device('cpu')
model = torch.load(opt.model_file, map_location=device)

if type(model) == dict:
    if 'full_model' in model.keys() and model['full_model'] is not None:
        model = model['full_model']
    elif 'model_state' in model.keys() and model['model_state'] is not None:
        if 'backbone_state' in model.keys():
            model = getModifiedFasterRCNN(backbone_state=model['backbone_state'], model_state=model['model_state'])
        else:
            model = getModifiedFasterRCNN(model_state=model['model_state'])

model = model.to(device)
model.eval()
#===========================================

def extractFaceImages(frame, boxes):
    extractedImages = []
    for i in range(len(boxes)):
        y1, y2 = int(round(boxes[i][1])), int(round(boxes[i][3]))
        x1, x2 = int(round(boxes[i][0])), int(round(boxes[i][2]))
        extractedImg = frame[y1:y2, x1:x2]
        # cv2.imshow('Face{}'.format(i), extractedImg)
        extractedImages.append(extractedImg)

    return extractedImages

#===========================================
#If source is image, open and predict
if opt.in_type == 'img':
    image = cv2.imread(opt.input_file)

    if image is None:
        print("Error opening file", opt.input_file, "as image.")
        exit(1)

    app = QtWidgets.QApplication([])
    application = MyGUI(opt.output_root, 
                    model, 
                    image, 
                    namesList, 
                    opt.in_type, 
                    device,
                    opt.score_threshold,
                    opt.iou_threshold, 
                    opt.model_input_size)

    application.show()
    sys.exit(app.exec())

elif opt.in_type == 'v':
    cap = cv2.VideoCapture(opt.input_file)
    if not cap.isOpened():
        print("Error opening video file", opt.input_file)
        exit(1)

    if (cap.isOpened()):
        app = QtWidgets.QApplication([])
        application = MyGUI(opt.output_root, 
                model, 
                cap, 
                namesList, 
                opt.in_type, 
                device,
                opt.score_threshold,
                opt.iou_threshold,
                opt.model_input_size)

        application.show()
        sys.exit(app.exec())

    cap.release()

cv2.destroyAllWindows()